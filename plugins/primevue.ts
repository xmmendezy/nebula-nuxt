import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import Button from 'primevue/button';
import Dropdown from 'primevue/dropdown';
import InputText from 'primevue/inputtext';

/**
 * primevue - Nuxt plugin
 *
 * Inicializa e inyecta componentes de primevue a la app nuxt
 */
export default defineNuxtPlugin(async (nuxtApp) => {
	nuxtApp.vueApp.use(PrimeVue, { ripple: true });
	nuxtApp.vueApp.use(ToastService);
	nuxtApp.vueApp.component('PButton', Button);
	nuxtApp.vueApp.component('PDropdown', Dropdown);
	nuxtApp.vueApp.component('PInputText', InputText);
	//other components that you need
});
