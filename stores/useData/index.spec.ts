import { describe, it, expect } from 'vitest';

import useData from './';

/**
 * Test useData store
 *
 * Description
 */
describe('useData', async () => {
	it('is a defined', async () => {
		const store = useData();
		expect(store).toBeDefined();
		expect(store.data).toBe(100);
		store.increment();
		expect(store.data).toBe(200);
	});
});
