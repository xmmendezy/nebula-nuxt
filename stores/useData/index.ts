import { defineStore } from 'pinia';

/**
 * useData store
 *
 * Description
 */
export default defineStore('data', () => {
	const data = ref(100);

	const increment = () => {
		data.value = data.value + 100;
	};

	return { data, increment };
});
