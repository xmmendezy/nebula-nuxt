import { describe, it, expect } from 'vitest';

import useValue from './';

/**
 * Test useValue composable
 *
 * Description
 */
describe('useValue', async () => {
	it('is a defined', async () => {
		const value = useValue();
		expect(value).toBeDefined();
		expect(value.data.value).toBe(0);
		value.data.value = 10;
		expect(value.data.value).toBe(10);
	});
});
