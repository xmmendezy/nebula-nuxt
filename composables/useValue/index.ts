/**
 * useValue composable
 *
 * Description
 */
export default () => {
	const data = ref(0);
	return { data };
};
