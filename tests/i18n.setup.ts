import { vi, Mock } from 'vitest';
import { config } from '@vue/test-utils';
import { useI18n } from 'vue-i18n';

vi.mock('vue-i18n');

(useI18n as Mock).mockReturnValue({
	t: (msg: string) => msg,
});

config.global.mocks = {
	$t: (msg: string) => msg,
};
