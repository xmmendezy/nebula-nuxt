import { config } from '@vue/test-utils';

// eslint-disable-next-line vue/one-component-per-file
const Component = defineComponent({
	setup() {
		return () => h('div', null);
	},
});

// eslint-disable-next-line vue/one-component-per-file,no-unused-vars
const ComponentSlot = defineComponent({
	setup(props, { slots }) {
		let slot_default: any = null;
		if (slots && slots.default) {
			slot_default = slots.default();
		}
		return () => h('div', slot_default);
	},
});

config.global.components = {
	PButton: Component,
	PDropdown: Component,
	PInputText: Component,
};
