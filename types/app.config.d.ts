interface NebulaAppConfig {
	appName: string;
}

declare module 'nuxt/schema' {
	// eslint-disable-next-line no-unused-vars
	interface AppConfigInput extends NebulaAppConfig {}

	// eslint-disable-next-line no-unused-vars
	interface AppConfig extends NebulaAppConfig {}
}

export {};
