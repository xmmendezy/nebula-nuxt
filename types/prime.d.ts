import Button from 'primevue/button';
import Dropdown from 'primevue/dropdown';
import InputText from 'primevue/inputtext';

declare module 'vue' {
	export interface GlobalComponents {
		PButton: typeof Button;
		PDropdown: typeof Dropdown;
		PInputText: typeof InputText;
	}
}

export const PButton: typeof Button;
export const PDropdown: typeof Dropdown;
export const PInputText: typeof InputText;
