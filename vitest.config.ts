import { defineVitestConfig } from 'nuxt-vitest/config';

export default defineVitestConfig({
	test: {
		environment: 'nuxt',
		setupFiles: ['tests/i18n.setup.ts', 'tests/prime.setup.ts'],
		coverage: {
			reporter: ['json-summary', 'text-summary', 'text'],
		},
	},
});
