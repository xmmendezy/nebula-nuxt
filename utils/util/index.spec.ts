import { describe, it, expect } from 'vitest';

import util from './';

/**
 * Test util
 *
 * Description
 */
describe('util', async () => {
	it('is a defined', async () => {
		const value = util();
		expect(value).toBeDefined();
		expect(typeof value).toBe('number');
	});
});
