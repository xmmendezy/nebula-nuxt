import { ModuleOptions } from '@nuxtjs/color-mode';

const colorModeConfig: Partial<ModuleOptions> = {
	preference: 'system',
	fallback: 'light',
	hid: 'nuxt-color-mode-script',
	globalName: '__NUXT_COLOR_MODE__',
	componentName: 'ColorScheme',
	classPrefix: '',
	classSuffix: '-mode',
	storageKey: 'nuxt-color-mode',
};

export default colorModeConfig;
