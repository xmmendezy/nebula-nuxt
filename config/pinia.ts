import { ModuleOptions } from '@pinia/nuxt';

const piniaConfig: Partial<ModuleOptions> = {
	autoImports: ['defineStore', ['defineStore', 'definePiniaStore']],
};

export default piniaConfig;
