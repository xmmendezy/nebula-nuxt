import { ModuleOptions } from '@nuxtjs/device/dist/module';

const deviceConfig: Partial<ModuleOptions> = {
	refreshOnResize: true,
};

export default deviceConfig;
