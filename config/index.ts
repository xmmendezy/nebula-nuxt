import piniaConfig from './pinia';
import i18nConfig from './i18n';
import colorModeConfig from './colorMode';
import deviceConfig from './device';

const modules = ['@vueuse/nuxt', '@pinia/nuxt', '@nuxtjs/device', 'nuxt-vitest', '@nuxtjs/i18n', '@nuxt/image'];

if (!process.env?.TEST) {
	modules.push('@nuxtjs/color-mode');
}

export default defineNuxtConfig({
	devtools: { enabled: true },
	app: {
		head: {
			title: 'Nebula Nuxt',
		},
	},
	components: [{ path: '~/components', prefix: 'K' }],
	modules,
	pinia: piniaConfig,
	i18n: i18nConfig,
	colorMode: colorModeConfig,
	device: deviceConfig,
	css: [
		'@fontsource/poppins/index.css',
		'@/assets/scss/main.scss',
		'primevue/resources/primevue.css',
		'primeicons/primeicons.css',
		'primeflex/primeflex.css',
	],
	build: {
		transpile: ['primevue'],
	},
});
