import { loadNuxt, buildNuxt } from '@nuxt/kit';
import { InlineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

export async function viteConfig() {
	const nuxt = await loadNuxt({});
	return new Promise<InlineConfig>((resolve, reject) => {
		nuxt.hook('vite:extendConfig', (config) => {
			if (config) {
				config.optimizeDeps = {
					disabled: true,
				};
				(config.plugins || []).push(
					vue({
						template: {
							compilerOptions: {
								isCustomElement: (tag) => tag.includes('-'),
							},
						},
					}),
				);
				if (config.server) {
					config.server.middlewareMode = false;
				}
			}
			resolve(config);
			throw new Error('_stop_');
		});
		buildNuxt(nuxt).catch((err) => {
			if (!err.toString().includes('_stop_')) {
				reject(err);
			}
		});
	}).finally(() => nuxt.close());
}
