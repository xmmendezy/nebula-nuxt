import { NuxtI18nOptions } from '@nuxtjs/i18n';

const i18nConfig: Partial<NuxtI18nOptions> = {
	locales: [
		{
			code: 'es',
			file: 'es-ES.json',
		},
		{
			code: 'en',
			file: 'en-US.json',
		},
		{
			code: 'pt',
			file: 'pt-PT.json',
		},
	],
	strategy: 'no_prefix',
	langDir: 'lang',
	defaultLocale: 'es',
};

export default i18nConfig;
