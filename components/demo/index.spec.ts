import { describe, it, expect } from 'vitest';
import { mountSuspended } from 'nuxt-vitest/utils';

import Demo from './index.vue';

/**
 * Test Demo component
 *
 * Description
 */
describe('Demo', async () => {
	it('is a Vue instance', async () => {
		const wrapper = await mountSuspended(Demo);
		expect(wrapper.vm).toBeTruthy();
	});

	it('content', async () => {
		const wrapper = await mountSuspended(Demo);
		expect(wrapper.text()).toBe('Demo Component Nebula Nuxt 0 welcome');
	});
});
